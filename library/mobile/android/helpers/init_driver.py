from library.mobile.android.helpers.init_mobile_config import InitMobileConfig
from appium import webdriver
import pytest


def start_app():
    desired_cap = {"deviceName": InitMobileConfig.deviceName,
                   "platformName": "Android",
                   "autoGrantPermissions": "true",
                   "appActivity": "ru.handh.android.boxberry.ui.activity.MainActivity",
                   "app": InitMobileConfig.app,
                   "unicodeKeyboard": True,
                   "resetKeyboard": True}

    android_driver = webdriver.Remote(InitMobileConfig.server, desired_cap)
    return android_driver


@pytest.fixture(scope='session')
def driver():
    android_driver = start_app()
    android_driver.implicitly_wait(10)
    yield android_driver
    android_driver.quit()


@pytest.fixture(scope="function")
def open_close_app_per_test(driver):
    app_state = driver.query_app_state('ru.boxberry.mobile.dev')  # 1 - приложение закрыто, 4 - активно
    if app_state == 1 or app_state == 3:
        driver.launch_app()

    yield
    driver.close_app()
