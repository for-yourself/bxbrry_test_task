import allure
import pytest
from library.mobile.android.helpers.helper_base import HelperBase


class SplashScreenHelper(HelperBase):

    def check_splash_screen(self):
        with allure.step('Проверка сплэш-экрана(анимации) при запуске приложения'):
            self.find_by_id('ru.boxberry.mobile.dev:id/animationView')

    def check_splash_screen_after_initial_installation(self):
        self.check_splash_screen()
        with allure.step('Проверка появления экрана авторизации при первичном запуске приложения'):
            self.find_by_id('ru.boxberry.mobile.dev:id/buttonSubmit')


@pytest.fixture()
def authorization_helper_api(driver):
    return SplashScreenHelper(driver)
