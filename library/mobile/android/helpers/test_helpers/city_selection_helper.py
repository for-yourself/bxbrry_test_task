import allure
import pytest
from library.mobile.android.helpers import HelperBase


class CitySelectionHelper(HelperBase):

    def check_city_selection_at_first_entrance(self):
        with allure.step('Переход на экран "Ваш город" с последующей проверкой элементов'):
            skip_btn_is_present = self.is_element_present_by_id('ru.boxberry.mobile.dev:id/buttonSkip')

            if skip_btn_is_present:
                skip_btn = self.find_by_id('ru.boxberry.mobile.dev:id/buttonSkip')
                self.click_by_element(skip_btn)

            with allure.step('Проверка экрана "Ваш город"'):
                self.check_city_selection_screen('Ваш город')

            with allure.step('Проверка перехода на главный экран'):
                self.find_by_id('ru.boxberry.mobile.dev:id/frameLayoutSearch')

    def check_city_selection_via_user_profile(self):
        with allure.step('Переход на экран "Ваш город" через профиль пользователя с последующей проверкой элементов'):
            self.skip_init_screens()

            more_btn = self.find_by_id('ru.boxberry.mobile.dev:id/more')
            self.click_by_element(more_btn)

            our_city_btn = self.find_by_id('ru.boxberry.mobile.dev:id/layoutCity')
            self.click_by_element(our_city_btn)

            with allure.step('Проверка экрана "Ваш город"'):
                self.check_city_selection_screen('Ваш город')

            with allure.step('Проверка установленного города'):
                specific_city_placeholder = self.find_by_id('ru.boxberry.mobile.dev:id/textViewCityPlaceholder')
                self.check_text_by_element(specific_city_placeholder, 'Сочи', 'Город не соответствует установленному')

    def check_city_selection_via_the_parcel_registration_screen(self):
        with allure.step('Переход на экран выбора города (отправления/поолучения) с последующей проверкой элементов'):
            self.skip_init_screens()

            parcel_calc_btn = self.find_by_id('ru.boxberry.mobile.dev:id/frameLayoutCalculator')
            self.click_by_element(parcel_calc_btn)

            with allure.step('Проверка экрана "Город отправления"'):
                edit_city_from_btn = self.find_by_id('ru.boxberry.mobile.dev:id/editTextCityFrom')
                self.click_by_element(edit_city_from_btn)
                self.check_city_selection_screen('Город отправления')

            with allure.step('Проверка экрана "Город получения"'):
                edit_city_to_btn = self.find_by_id('ru.boxberry.mobile.dev:id/editTextCityTo')
                self.click_by_element(edit_city_to_btn)
                self.check_city_selection_screen('Город получения')

    def skip_init_screens(self):
        skip_btn_is_present = self.is_element_present_by_id('ru.boxberry.mobile.dev:id/buttonSkip')

        if skip_btn_is_present:
            skip_btn = self.find_by_id('ru.boxberry.mobile.dev:id/buttonSkip')
            self.click_by_element(skip_btn)

            specific_city_btn_present = self.is_element_present_by_xpath(
                "//android.widget.RelativeLayout/android.widget.TextView[@text='Краснодар']")

            if specific_city_btn_present:
                specific_city_btn = self.find_by_xpath("//android.widget.RelativeLayout/android.widget.TextView\
                                                        [@text='Краснодар']")
                self.click_by_element(specific_city_btn)

    def check_city_selection_screen(self, page_header):
        with allure.step('Проверка текста заголовка экрана'):
            city_selection_page_header = self.find_by_xpath("//android.widget.LinearLayout/android.view.ViewGroup\
            [@resource-id='ru.boxberry.mobile.dev:id/toolbar']/android.widget.TextView")
            self.check_text_by_element(city_selection_page_header, page_header,
                                       f'Заголовок экрана {page_header} некорректен')

            # немного проскролить экран:
            self.swipe_from_down_to_up()

        with allure.step('Проверка строки поиска города'):
            search_btn = self.find_by_id('ru.boxberry.mobile.dev:id/actionSearch')
            self.click_by_element(search_btn)

            with allure.step('Проверка плейсхолдера поля "Поиск города"'):
                search_field = self.find_by_id('ru.boxberry.mobile.dev:id/editTextSearch')
                self.check_text_by_element(search_field, 'Поиск города', f'Плейсхолдер поля "Поиск города" некорректен')

            with allure.step('Проверка кнопки назад'):
                search_back_btn = self.find_by_xpath("//android.widget.LinearLayout/android.view.ViewGroup\
                [@resource-id='ru.boxberry.mobile.dev:id/toolbar']/android.widget.ImageButton")
                self.click_by_element(search_back_btn)
                self.wait_for_element_to_be_clickable_by_id('ru.boxberry.mobile.dev:id/actionSearch')
                self.click_by_element(search_btn)

            with allure.step('Проверка кнопки очистки поля "Поиск города"'):
                self.set_value(search_field, 'Сочи')
                search_close_btn = self.find_by_id('ru.boxberry.mobile.dev:id/actionClear')
                self.click_by_element(search_close_btn)
                self.check_text_by_element(search_field, 'Поиск города', 'Поле "Поиск города" не очистилось')

            with allure.step('Ввести некорректные данные'):
                self.set_value(search_field, '12345')
                assert self.get_text_by_id('ru.boxberry.mobile.dev:id/textViewErrorMessage') == 'Ничего не найдено', \
                                            'Текст ошибки отсутствует/отличается'

                clear_input_data_btn = self.find_by_id('ru.boxberry.mobile.dev:id/buttonClear')
                self.click_by_element(clear_input_data_btn)

            with allure.step('Выбор города'):
                self.set_value(search_field, 'Сочи')
                search_result = self.find_by_id('ru.boxberry.mobile.dev:id/textViewTitle')
                self.check_text_by_element(search_result, 'Сочи', 'Результат поиска города некорректен')
                self.click_by_element(search_result)


@pytest.fixture()
def city_selection_helper_api(driver):
    return CitySelectionHelper(driver)

