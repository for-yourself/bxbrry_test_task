from configparser import ConfigParser
import os


class InitMobileConfig:

    path_current_directory = os.path.dirname(__file__)
    path_config_file = os.path.join(path_current_directory, '', 'mobile_config.ini')
    config = ConfigParser()
    config.read(path_config_file, 'utf-8')

    deviceName = config.get('capabilities', 'deviceName')
    app = config.get('capabilities', 'app')
    server = config.get('capabilities', 'server')
