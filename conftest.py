from helpers.init_config import InitConfig
from helpers.github_api import GitHubApi
from library.mobile.android.helpers import *
from library.mobile.android.helpers.test_helpers import *
import pytest


@pytest.fixture
def github_api():
    return GitHubApi(base_address=InitConfig.clientUrl)
