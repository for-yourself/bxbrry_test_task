import allure


class TestCitySelection:

    @allure.feature('Android')
    @allure.story('Тестирование экрана "Выбор города"')
    @allure.title('Тестирование экрана "Выбор города" после первичной установки')
    def test_city_selection_at_first_entrance(self, driver, open_close_app_per_test, city_selection_helper_api):
        city_selection_helper_api.check_city_selection_at_first_entrance()

    @allure.feature('Android')
    @allure.story('Тестирование экрана "Выбор города"')
    @allure.title('Тестирование экрана "Выбор города" через экран профиля пользователя')
    def test_city_selection_via_user_profile(self, driver, open_close_app_per_test, city_selection_helper_api):
        city_selection_helper_api.check_city_selection_via_user_profile()

    @allure.feature('Android')
    @allure.story('Тестирование экрана "Выбор города"')
    @allure.title('Тестирование экрана "Выбор города" через экран оформления посылки')
    def test_city_selection_via_the_parcel_registration_screen(self, driver, open_close_app_per_test,
                                                               city_selection_helper_api):
        city_selection_helper_api.check_city_selection_via_the_parcel_registration_screen()
