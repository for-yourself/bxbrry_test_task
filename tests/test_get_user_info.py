from helpers.init_config import InitConfig
import allure


def test_get_response_status(github_api):
    response = github_api.get("users/maksim-smkv")

    with allure.step("Проверка кода ответа"):
        assert response.status_code == 200


def test_get_user_login(github_api):
    response = github_api.get("users/maksim-smkv")
    json_response = response.json()

    with allure.step("Проверим, что ответ содержит верный login"):
        assert json_response['login'] == InitConfig.login
        print(json_response['login'])


def test_get_user_id(github_api):
    response = github_api.get("users/maksim-smkv")
    json_response = response.json()

    with allure.step("Проверим, что ответ содержит верный id"):
        assert json_response['id'] == int(InitConfig.id)
        print(json_response['id'])
