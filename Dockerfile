FROM python:3.8-slim-buster

WORKDIR /usr/src/bb
COPY . .
RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y git
RUN apt-get install -y python3-dev
RUN apt-get install -y libevent-dev
RUN apt-get install -y libblas-dev libatlas-base-dev
RUN apt-get install -y gcc
RUN python -m pip install --upgrade pip
COPY requirements.txt /
RUN pip install -r requirements.txt
RUN mkdir allure-results
RUN mkdir allure-report
