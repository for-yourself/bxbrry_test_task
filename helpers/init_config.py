from configparser import ConfigParser
from pathlib import Path
import os


class InitConfig:

    path_current_directory = os.path.dirname(__file__)
    proj_root_directory = Path(path_current_directory).parent
    path_config_file = os.path.join(proj_root_directory, '', 'config.ini')
    config = ConfigParser()
    config.read(path_config_file, 'utf-8')

    clientUrl = config.get('auth', 'clientUrl')
    login = config.get('auth', 'login')
    id = config.get('auth', 'id')
